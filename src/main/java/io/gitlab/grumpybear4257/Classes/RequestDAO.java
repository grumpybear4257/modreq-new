package io.gitlab.grumpybear4257.Classes;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class RequestDAO extends BasicDAO<Request, String> {
    public RequestDAO(Class<Request> entityClass, Datastore ds) {
        super(entityClass, ds);
    }
}
