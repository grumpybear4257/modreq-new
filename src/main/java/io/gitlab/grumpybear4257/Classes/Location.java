package io.gitlab.grumpybear4257.Classes;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class Location {
    public String WorldUUID;

    public double X;
    public double Y;
    public double Z;
    public float yaw;
    public float pitch;

    public Location(org.bukkit.Location location) {
        if (location.isWorldLoaded() && location.getWorld() != null)
            WorldUUID = location.getWorld().getUID().toString();

        X = location.getX();
        Y = location.getY();
        Z = location.getZ();

        yaw = location.getYaw();
        pitch = location.getPitch();
    }

    private Location() {

    }
}
