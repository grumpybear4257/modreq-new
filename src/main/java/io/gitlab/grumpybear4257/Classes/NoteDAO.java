package io.gitlab.grumpybear4257.Classes;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class NoteDAO extends BasicDAO<Note, String> {
    public NoteDAO(Class<Note> entityClass, Datastore ds) {
        super(entityClass, ds);
    }
}
