package io.gitlab.grumpybear4257.Classes.Enums;

public enum Status {
    Open,
    Accepted,
    Resolved,
    Closed,
    Escalated
}
