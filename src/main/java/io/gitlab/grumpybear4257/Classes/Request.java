package io.gitlab.grumpybear4257.Classes;

import io.gitlab.grumpybear4257.Classes.Enums.Status;
import io.gitlab.grumpybear4257.Events.RequestSubmittedEvent;
import io.gitlab.grumpybear4257.Utils.BukkitUtils;
import io.gitlab.grumpybear4257.MRUtils.Database;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import java.util.Date;
import java.util.UUID;

@Entity(value = "Requests", noClassnameStored = true)
public class Request {
    @Id
    public long Id;

    @Indexed
    public Status ReqStatus;

    public String Description;

    @Indexed
    public String SubmitterUUID;

    @Indexed
    public String AssigneeUUID;

    public Date SubmittedDate;

    public Date ResolutionDate;

    public Date ModifiedDate;

    @Embedded
    public Location Location;

    // For Morphia
    private Request() {
    }

    public static Request Create(String description, Player submitter) {
        Request request = new Request();
        request.Id = Database.TotalNumberOfRequests();
        request.ReqStatus = Status.Open;
        request.Description = description;
        request.SubmitterUUID = submitter.getUniqueId().toString();
        request.SubmittedDate = new Date();
        request.Location = new Location(submitter.getLocation());

        Database.CreateRequest(request);

        RequestSubmittedEvent submittedEvent = new RequestSubmittedEvent(request, submitter);
        Bukkit.getPluginManager().callEvent(submittedEvent);

        BukkitUtils.SendToAllPlayersWithPermission("modreq.viewQueue", ChatColor.GOLD + "New mod request from " + ChatColor.AQUA + BukkitUtils.PrintPlayerName(submitter) + ChatColor.GOLD + " (#" + request.Id + ")");
        return request;
    }

    public UUID getSubmitterUUID() {
        try {
            return UUID.fromString(SubmitterUUID);
        } catch (Exception e) {
            return null;
        }
    }

    public UUID getAssigneeUUID() {
        try {
            return UUID.fromString(AssigneeUUID);
        } catch (Exception e) {
            return null;
        }
    }
}

