package io.gitlab.grumpybear4257.Classes;

import io.gitlab.grumpybear4257.Events.NoteCreatedEvent;
import io.gitlab.grumpybear4257.MRUtils.Database;
import io.gitlab.grumpybear4257.Utils.BukkitUtils;
import io.gitlab.grumpybear4257.Utils.CommonUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;

import java.util.Date;
import java.util.UUID;

@Entity(value = "Notes", noClassnameStored = true)
public class Note {
    @Id
    public long Id;

    @Indexed @Reference
    public Request Request;

    @Indexed
    public String AuthorUUID;

    public String Content;

    public Date CreatedDate;

    // For Morphia
    private Note() {
    }

    public static Note Create(Request request, @org.jetbrains.annotations.Nullable Player author, String content) {
        Note note = new Note();
        note.Id = Database.TotalNumberOfNotes();
        note.Request = request;
        note.AuthorUUID = author != null ? author.getUniqueId().toString() : "CONSOLE";
        note.Content = content;
        note.CreatedDate = new Date();

        Database.CreateNote(note);
        NoteCreatedEvent noteCreatedEvent = new NoteCreatedEvent(note, author);
        Bukkit.getPluginManager().callEvent(noteCreatedEvent);

        note.sendNotification();

        return note;
    }

    public static Note CreateConsoleNote(Request request, String content) {
        return Create(request, null, content);
    }

    private void sendNotification() {
        UUID uuid = CommonUtils.GetUUIDFromString(AuthorUUID);
        String author = BukkitUtils.PrintPlayerName(uuid);
        // String author = uuid != null ? Bukkit.getOfflinePlayer(uuid).getName() : Bukkit.getConsoleSender().getName();

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getUniqueId().toString().equals(Request.SubmitterUUID) || player.getUniqueId().toString().equals(Request.AssigneeUUID))
                player.sendMessage(ChatColor.GOLD + "A new note has been received by " + ChatColor.AQUA + author + ChatColor.GOLD + " on request " + ChatColor.AQUA + "#" + Request.Id + ChatColor.GOLD + "." +
                        "\nDo " + ChatColor.WHITE + "/request notes " + Request.Id + ChatColor.GOLD + " to view all notes on the request.");
        }
    }
}

