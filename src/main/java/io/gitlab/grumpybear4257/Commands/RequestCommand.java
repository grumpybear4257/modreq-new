package io.gitlab.grumpybear4257.Commands;

import io.gitlab.grumpybear4257.Classes.Enums.Status;
import io.gitlab.grumpybear4257.Classes.Note;
import io.gitlab.grumpybear4257.Classes.Request;
import io.gitlab.grumpybear4257.Utils.BukkitUtils;
import io.gitlab.grumpybear4257.Utils.CommonUtils;
import io.gitlab.grumpybear4257.MRUtils.Database;
import io.gitlab.grumpybear4257.MRUtils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class RequestCommand implements CommandExecutor, TabExecutor {
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        // Check to see if the sender isn't a player, or the console
        if (!(sender instanceof Player) && !(sender instanceof ConsoleCommandSender)) {
            sender.sendMessage(ChatColor.RED + "Invalid sender");
            return true;
        }

        if (args.length == 0) {
            BukkitUtils.BadSyntax(sender, "/request [subcommand] <parameters>");
            return true;
        }

        String subCommand = args[0];
        args = Arrays.copyOfRange(args, 1, args.length);

        // All player + console commands go here
        if (subCommand.equalsIgnoreCase("view"))
            return requestView(sender, args);
        else if (subCommand.equalsIgnoreCase("close"))
            return requestClose(sender, args);
        else if (subCommand.equalsIgnoreCase("notes"))
            return requestNotes(sender, args);

        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Not a player");
            return true;
        }

        Player player = (Player) sender;

        // All player only commands go here.
        if (subCommand.equalsIgnoreCase("create"))
            return requestCreate(player, args);
        else if (subCommand.equalsIgnoreCase("resolve"))
            return requestResolve(player, args);
        else if (subCommand.equalsIgnoreCase("abandon"))
            return requestAbandon(player, args);
        else if (subCommand.equalsIgnoreCase("assign"))
            return requestAssign(player, args);
        else if (subCommand.equalsIgnoreCase("queue"))
            return new QueueCommand().onCommand(player, command, label, args);

        return false;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        // lmao this method is hot garbage since it's basically all just repeated crap.
        // I should probably move all the subcommands out of this file, both so that the file isn't massive,
        // but also so this method isn't nearly as bad
        Player player = (Player) sender; //FIXME so console works
        List<String> options = new ArrayList<String>();
        if (args.length == 1) {
            BukkitUtils.AddTabCompleteOption(player, alias, options, "create", args[0]);
            BukkitUtils.AddTabCompleteOption(player, alias, options, "view", args[0]);
            BukkitUtils.AddTabCompleteOption(player, alias, options, "resolve", args[0]);
            BukkitUtils.AddTabCompleteOption(player, alias, options, "close", args[0]);
            BukkitUtils.AddTabCompleteOption(player, alias, options, "abandon", args[0]);
            BukkitUtils.AddTabCompleteOption(player, alias, options, "assign", args[0]);
            BukkitUtils.AddTabCompleteOption(player, alias, options, "queue", args[0]);
            BukkitUtils.AddTabCompleteOption(player, alias, options, "notes", args[0]);
        } else if (args.length == 2) {
            if (args[0].equalsIgnoreCase("create")) {
                return null;
            } else if (args[0].equalsIgnoreCase("view")) {
                // Return nothing if they don't have this permission node
                if (!player.hasPermission("modreq.request.view"))
                    return options;

                if (player.hasPermission("modreq.request.accept")) {
                    // They can accept requests, Display open requests that are assigned to them.
                    for (Request request : Database.GetAssignedRequests(player))
                        BukkitUtils.AddTabCompleteOption(options, Long.toString(request.Id), args[1]);
                } else if (player.hasPermission("modreq.request.view")) {
                    // They can see all player request. Display open requests in the queue.
                    for (Request request : Database.GetOpenRequestsInQueue())
                        BukkitUtils.AddTabCompleteOption(options, Long.toString(request.Id),args[1]);
                } else {
                    // They can only see their requests.
                    for (Request request : Database.GetOpenCreatedRequests(player))
                        BukkitUtils.AddTabCompleteOption(options, Long.toString(request.Id), args[1]);
                }
            } else if (args[0].equalsIgnoreCase("resolve")) {
                // Return nothing if they don't have this permission node
                if (!player.hasPermission("modreq.request.resolve"))
                    return options;

                //TODO
            } else if (args[0].equalsIgnoreCase("close")) {
                // Return nothing if they don't have this permission node
                if (!player.hasPermission("modreq.request.close"))
                    return options;

                for (Request request : Database.GetOpenRequestsInQueue())
                    BukkitUtils.AddTabCompleteOption(options, Long.toString(request.Id), args[1]);
            } else if (args[0].equalsIgnoreCase("abandon")) {
                // Return nothing if they don't have this permission node
                if (!player.hasPermission("modreq.request.abandon"))
                    return options;

                for (Request request : Database.GetAssignedRequests(player))
                    BukkitUtils.AddTabCompleteOption(options, Long.toString(request.Id), args[1]);
            } else if (args[0].equalsIgnoreCase("assign")) {
                // Return nothing if they don't have this permission node
                if (!player.hasPermission("modreq.request.assign"))
                    return options;

                //TODO
            } else if (args[0].equalsIgnoreCase("queue")) {
                return new QueueCommand().onTabComplete(sender, command, alias, Arrays.copyOfRange(args, 1, args.length));
            } else if (args[0].equalsIgnoreCase("notes")) {
                if (!player.hasPermission("modreq.request.notes"))
                    return options;

                // TODO get all requests that are either assigned to the player, or that are open & assigned to the player (if applicable)
            }
        }

        return options;
    }

    private boolean requestView(CommandSender sender, String[] args) {
        if (!BukkitUtils.CheckPlayerPermission(sender, "modreq.request.view"))
            return true;

        if (args.length != 1) {
            BukkitUtils.BadSyntax(sender, "/request view [id]");
            return true;
        }

        Request request = Database.GetRequest(Utils.GetRequestId(sender, args[0]));
        UUID uuid = request.getSubmitterUUID();
        String submitterName = uuid == null ? Bukkit.getConsoleSender().getName() : Bukkit.getOfflinePlayer(uuid).getName();
        StringBuilder message = new StringBuilder(ChatColor.GOLD + "Viewing Information For Request " + ChatColor.AQUA + "#" + request.Id +
                ChatColor.GOLD + "\nStatus: " + ChatColor.AQUA + request.ReqStatus +
                ChatColor.GOLD + "\nSubmitter: " + ChatColor.AQUA + submitterName +
                ChatColor.GOLD + "\nSubmitted At: " + ChatColor.AQUA + request.SubmittedDate.toString());

        if (request.getAssigneeUUID() != null)
            message.append(ChatColor.GOLD + "\nAssigned To: " + ChatColor.AQUA + Bukkit.getOfflinePlayer(request.getAssigneeUUID()).getName());

        if (request.ModifiedDate != null)
            message.append(ChatColor.GOLD + "\nLast Updated On: " + ChatColor.AQUA + request.ModifiedDate.toString());

        if (request.ResolutionDate != null)
            message.append(ChatColor.GOLD + "\nResolved On: " + ChatColor.AQUA + request.ResolutionDate.toString());

        List<Note> notes = Database.GetNotesForRequest(request);
        if (notes.size() > 0)
            message.append(ChatColor.GOLD + "\nNotes Attached: " + ChatColor.AQUA + notes.size());

        message.append(ChatColor.GOLD + "\nRequest: " + ChatColor.AQUA + request.Description);

        sender.sendMessage(message.toString());

        return true;
    }

    private boolean requestClose(CommandSender sender, String[] args) {
        if (!BukkitUtils.CheckPlayerPermission(sender, "modreq.request.close"))
            return true;

        if (args.length < 2) {
            BukkitUtils.BadSyntax(sender, "/request close [id] [reason]");
            return true;
        }

        Request request = Database.GetRequest(Utils.GetRequestId(sender, args[0]));
        if (request.ReqStatus == Status.Closed || request.ReqStatus == Status.Resolved) {
            sender.sendMessage(ChatColor.RED + "That request is not open!");
            return true;
        }

        args = Arrays.copyOfRange(args, 1, args.length);

        request.ReqStatus = Status.Closed;
        Note note = sender instanceof Player ?
                Note.Create(request, (Player) sender, CommonUtils.GetString(args)) :
                Note.CreateConsoleNote(request, CommonUtils.GetString(args));

        Database.CreateNote(note);
        Database.UpdateRequest(request);

        return true;
    }

    private boolean requestNotes(CommandSender sender, String[] args) {
        if (args.length == 0) {
            BukkitUtils.BadSyntax(sender, "/request notes [request id] <note>");
            return true;
        }

        Request request = Database.GetRequest(Utils.GetRequestId(sender, args[0]));
        if (args.length == 1) {
            List<Note> notes = Database.GetNotesForRequest(request);
            if (notes.isEmpty()) {
                sender.sendMessage(ChatColor.RED + "That request does not have any notes attached!\n" +
                        "To attach a note do " + ChatColor.WHITE + "/request notes [request id] [note]");
                return true;
            } else {
                StringBuilder message = new StringBuilder(ChatColor.GOLD + "Viewing Notes for Request " + ChatColor.AQUA + "#" + request.Id);
                for (int i = 0; i < notes.size(); i++) {
                    Note note = notes.get(i);
                    message.append("\n");
                    message.append(ChatColor.AQUA + BukkitUtils.PrintPlayerName(CommonUtils.GetUUIDFromString(note.AuthorUUID)));

                    // Check to see if this is the last note and the request has been closed. If so, mark the note as the reason for the closure
                    if (request.ReqStatus == Status.Closed && i == notes.size() -1)
                        message.append(ChatColor.GOLD + " - " + ChatColor.RED + "CLOSED");

                    message.append(ChatColor.GOLD + ": " + note.Content);
                }

                sender.sendMessage(message.toString());
            }
        } else {
            if (!BukkitUtils.CheckPlayerPermission(sender, "modreq.request.notes"))
                return true;

            // Check to see if the request has been closed, if so, don't let them submit a new note to it.
            // Request marked as resolved can still have notes attached to them.
            if (request.ReqStatus == Status.Closed) {
                sender.sendMessage(ChatColor.RED + "That request has been closed!");
                return true;
            }

            args = Arrays.copyOfRange(args, 1, args.length);
            Note note = sender instanceof Player ?
                    Note.Create(request, (Player) sender, CommonUtils.GetString(args)) :
                    Note.CreateConsoleNote(request, CommonUtils.GetString(args));

            Database.CreateNote(note);
            Database.UpdateRequest(request);
        }

        return true;
    }

    private boolean requestCreate(Player player, String[] description) {
        if (!BukkitUtils.CheckPlayerPermission(player, "modreq.request.create"))
            return true;

        if (description.length == 0) {
            player.sendMessage(ChatColor.RED + "Invalid Syntax - Usage: " + ChatColor.WHITE + "/request create [request]");
            return true;
        }

        Request request = Request.Create(CommonUtils.GetString(description), player);
        player.sendMessage(ChatColor.GOLD + "Request created with ID " + ChatColor.AQUA + "#" + request.Id + ChatColor.GOLD + ".");

        return true;
    }

    private boolean requestResolve(Player player, String[] args) {
        if (args.length < 2) {
            BukkitUtils.BadSyntax(player, "/request resolve [id] [resolution]");
            return true;
        }

        long id = Utils.GetRequestId(player, args[0]);

        //TODO
        return true;
    }

    private boolean requestAbandon(Player player, String[] args) {
        if (args.length < 2) {
            BukkitUtils.BadSyntax(player, "/request abandon [id] [reason]");
            return true;
        }

        long id = Utils.GetRequestId(player, args[0]);

        //TODO
        return true;
    }

    private boolean requestAssign(Player player, String[] args) {
        if (args.length < 3) {
            BukkitUtils.BadSyntax(player, "/request assign [id] [target user] [reason]");
            return true;
        }

        long id = Utils.GetRequestId(player, args[0]);

        //TODO
        return true;
    }
}
