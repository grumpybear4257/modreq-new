package io.gitlab.grumpybear4257.Commands;

import io.gitlab.grumpybear4257.Utils.BukkitUtils;
import io.gitlab.grumpybear4257.Utils.VersionChecker;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ModreqCommand implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        // Check to see if there were any arguments to the command
        if (args.length == 0) {
            sender.sendMessage(ChatColor.GOLD + "This server is running " + ChatColor.GREEN + "Mod Request v" + VersionChecker.GetVersion() + ChatColor.GOLD + " by GrumpyBear57");

            // Inform the player how to submit a request
            if (sender.hasPermission("modreq.request.create"))
                sender.sendMessage(ChatColor.GOLD + "To create a new request, do " + ChatColor.WHITE + "/request create [request]");

            // Tell them to look at the help page for more command info
            sender.sendMessage(ChatColor.GOLD + "For a list of all available commands, please do " + ChatColor.WHITE + "/help modreq");

            return true;
        } else {
            BukkitUtils.BadSyntax(sender, "/modreq");
            return true;
        }
    }
}
