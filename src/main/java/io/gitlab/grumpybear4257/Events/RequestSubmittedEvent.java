package io.gitlab.grumpybear4257.Events;

import io.gitlab.grumpybear4257.Classes.Request;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RequestSubmittedEvent extends Event {
    private static final HandlerList HANDLERS = new HandlerList();

    private final Request request;
    private final Player submitter;

    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    public RequestSubmittedEvent(Request request, Player submitter) {
        this.request = request;
        this.submitter = submitter;
    }

    public Request getRequest() {
        return request;
    }

    public Player getSubmitter() {
        return submitter;
    }
}
