package io.gitlab.grumpybear4257.Events;

import io.gitlab.grumpybear4257.Classes.Note;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class NoteCreatedEvent extends Event {
    private static final HandlerList HANDLERS = new HandlerList();

    private final Note note;
    private final Player author;

    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public NoteCreatedEvent(Note note, Player author) {
        this.note = note;
        this.author = author;
    }

    public Note getNote() {
        return note;
    }

    public Player getAuthor() {
        return author;
    }
}
