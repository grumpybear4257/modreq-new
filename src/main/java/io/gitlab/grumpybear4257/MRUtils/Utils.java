package io.gitlab.grumpybear4257.MRUtils;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class Utils {
    public static long GetRequestId(CommandSender sender, String input) {
        try {
            return Long.parseLong(input);
        } catch (NumberFormatException nfe) {
            sender.sendMessage(ChatColor.RED + "Invalid request ID: " + input);
            return -1;
        }
    }
}
