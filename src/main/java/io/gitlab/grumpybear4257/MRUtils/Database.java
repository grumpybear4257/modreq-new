package io.gitlab.grumpybear4257.MRUtils;

import com.mongodb.MongoClient;
import io.gitlab.grumpybear4257.Classes.Enums.Status;
import io.gitlab.grumpybear4257.Classes.Note;
import io.gitlab.grumpybear4257.Classes.NoteDAO;
import io.gitlab.grumpybear4257.Classes.Request;
import io.gitlab.grumpybear4257.Classes.RequestDAO;
import org.bukkit.entity.Player;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import java.util.Date;
import java.util.List;

public class Database {
    private static MongoClient client;
    private static Morphia morphia;
    private static Datastore datastore;
    private static RequestDAO requestDAO;
    private static NoteDAO noteDAO;

    public static void Init() {
        client = new MongoClient();
        morphia = new Morphia();

        morphia.map(Request.class);
        morphia.map(Note.class);

        datastore = morphia.createDatastore(client, "minecraft");
        datastore.ensureIndexes();

        requestDAO = new RequestDAO(Request.class, datastore);
        noteDAO = new NoteDAO(Note.class, datastore);
    }

    public static void CreateRequest(Request request) {
        requestDAO.save(request);
    }

    public static void CreateNote(Note note) {
        noteDAO.save(note);
    }

    public static Request GetRequest(long requestId) {
        return requestDAO.findOne("Id", requestId);
    }

    public static long TotalNumberOfRequests() {
        return requestDAO.count();
    }

    public static long TotalNumberOfNotes() {
        return noteDAO.count();
    }

    public static List<Request> GetAssignedRequests(Player player) {
        Query<Request> query = datastore.createQuery(Request.class);
        query.field("AssigneeUUID").equal(player.getUniqueId().toString());
        query.field("ReqStatus").equal(Status.Accepted);

        return requestDAO.find(query).asList();
    }

    public static List<Request> GetOpenRequestsInQueue() {
        Query<Request> query = datastore.createQuery(Request.class);
        query.field("ReqStatus").equal(Status.Open);

        return requestDAO.find(query).asList();
    }

    public static List<Request> GetModifiedRequestsSinceLastLogin(Player player) {
        Query<Request> query = datastore.createQuery(Request.class);
        query.field("SubmitterUUID").equal(player.getUniqueId().toString());
        query.field("ModifiedDate").lessThan(new Date(player.getLastPlayed()));

        return requestDAO.find(query).asList();
    }

    public static List<Request> GetOpenCreatedRequests(Player player) {
        Query<Request> query = datastore.createQuery(Request.class);
        query.field("SubmitterUUID").equal(player.getUniqueId().toString());
        query.field("ReqStatus").equal(Status.Accepted);

        return requestDAO.find(query).asList();
    }

    public static void UpdateRequest(Request request) {
        request.ModifiedDate = new Date();
        requestDAO.save(request);
    }

    public static List<Note> GetNotesForRequest(Request request) {
        Query<Note> query = datastore.createQuery(Note.class);
        query.field("Request").equal(request);

        return noteDAO.find(query).asList();
    }
}
