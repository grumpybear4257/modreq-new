package io.gitlab.grumpybear4257.Listeners;

import io.gitlab.grumpybear4257.Classes.Request;
import io.gitlab.grumpybear4257.MRUtils.Database;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.List;

public class PlayerJoinListener implements Listener {
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        if (player.hasPermission("modreq.queue.view")) {
            long openRequests = Database.GetOpenRequestsInQueue().size();
            long assignedRequests = Database.GetAssignedRequests(player).size();

            if (openRequests == 0 && assignedRequests == 0)
                player.sendMessage(ChatColor.GOLD + "Hooray! You don't have any work to do!");
            else {
                String message = "There ";

                if (openRequests > 1)
                    message += "are " + openRequests + " open requests";
                else
                    message += "is " + openRequests + " open request";

                message += " in the queue. ";

                if (assignedRequests > 0)
                    message += "You have " + assignedRequests + " assigned to you.";

                player.sendMessage(ChatColor.GOLD + message);
            }

            return;
        }

        List<Request> modifiedRequests = Database.GetModifiedRequestsSinceLastLogin(player);
        if (modifiedRequests.size() > 0) {
            StringBuilder message = new StringBuilder("The following requests have been updated since you were last online: ");

            if (modifiedRequests.size() > 1)
                message = new StringBuilder(message.toString().replace("requests", "request"));

            for (int i = 0; i < modifiedRequests.size(); i++) {
                Request req = modifiedRequests.get(i);
                message.append(req.Id);

                if (i == modifiedRequests.size() - 1)
                    message.append(", ");
            }

            player.sendMessage(ChatColor.GOLD + message.toString());
        }
    }
}
