package io.gitlab.grumpybear4257;

import io.gitlab.grumpybear4257.Commands.ModreqCommand;
import io.gitlab.grumpybear4257.Commands.QueueCommand;
import io.gitlab.grumpybear4257.Commands.RequestCommand;
import io.gitlab.grumpybear4257.Listeners.PlayerJoinListener;
import io.gitlab.grumpybear4257.MRUtils.Database;
import io.gitlab.grumpybear4257.Utils.VersionChecker;
import org.bukkit.plugin.java.JavaPlugin;

public class Modreq extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();
        GBUtilLib.setPlugin(this);

        getLogger().info("Initializing Database");
        Database.Init();

        getLogger().info("Registering Commands");
        this.getCommand("modreq").setExecutor(new ModreqCommand());
        this.getCommand("request").setExecutor(new RequestCommand());
        this.getCommand("queue").setExecutor(new QueueCommand());

        getLogger().info("Registering Listeners");
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this );

        if (VersionChecker.UpdateAvailable()) {
            //TODO: Add download link, preferably one that directly links to the new version
            getLogger().info("An update for Modreq is available");
        }
    }
}

